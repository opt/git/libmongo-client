Source: libmongo-client
Priority: optional
Maintainer: Jörg Frings-Fürst <debian@jff.email>
Build-Depends:
 debhelper (>= 11~),
 libglib2.0-dev,
 perl
Build-Depends-Indep:
 doxygen,
 graphviz
Standards-Version: 4.2.1
Section: libs
Homepage: https://github.com/algernon/libmongo-client
Vcs-Git: git://jff.email/opt/git/libmongo-client.git
Vcs-Browser: https://jff.email/cgit/libmongo-cliet.git

Package: libmongo-client0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Alternate C driver for the MongoDB document-oriented datastore
 MongoDB is a high-performance, open source, schema-free
 document-oriented data store.
 .
 This library provides an alternative C language driver, focusing on
 stability, ease of use, striving to make the most common use cases as
 convenient as possible.
 .
 Among its features are:
 .
   * Well documented, easy, clean and stable API.
   * Support for asynchronous operation.
   * ReplicaSet support, with support for automatic reconnecting and
   discovery.
   * Safe-mode support, to optionally enable extra safety checks on
   writes.

Package: libmongo-client-dev
Section: libdevel
Architecture: any
Depends:
 libmongo-client0 (= ${binary:Version}),
 pkg-config,
 libglib2.0-dev,
 ${misc:Depends}
Description: Development files for the alternate C driver for MongoDB
 libmongo-client is an alternative C language driver to the MongoDB
 document-oriented datastore.
 .
 This package is needed to compile programs against libmongo-client0,
 as only it includes the header files and static libraries needed for
 compiling.

Package: libmongo-client-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: libjs-jquery
Description: Documentation for the alternate C driver for MongoDB
 libmongo-client is an alternative C language driver to the MongoDB
 document-oriented datastore.
 .
 This package contains the API documentation, tutorials and examples.
